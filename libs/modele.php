<?php
include_once("maLibSQL.pdo.php"); 
// définit les fonctions SQLSelect, SQLUpdate...


// Users ///////////////////////////////////////////////////

function validerUser($pseudo, $pass){
	$SQL = "SELECT id FROM users WHERE pseudo='$pseudo' AND pass='$pass'";
	if ($id=SQLGetChamp($SQL))
		return getHash($id);
	else return false;
}

function hash2id($hash) {
	$SQL = "SELECT id FROM users WHERE hash='$hash'";
	return SQLGetChamp($SQL); 
}

function hash2pseudo($hash) {
	$SQL = "SELECT pseudo FROM users WHERE hash='$hash'";
	return SQLGetChamp($SQL); 
}

function getUsers(){
	$SQL = "SELECT id,pseudo FROM users";
	return parcoursRs(SQLSelect($SQL));
}

function getUser($idUser){
	$SQL = "SELECT id,pseudo FROM users WHERE id='$idUser'";
	$rs = parcoursRs(SQLSelect($SQL));
	if (count($rs)) return $rs[0]; 
	else return array();
}

function getHash($idUser){
	$SQL = "SELECT hash FROM users WHERE id='$idUser'";
	return SQLGetChamp($SQL);
}

function mkHash($idUser) {
	// génère un (nouveau) hash pour cet user
	// il faudrait ajouter une date d'expiration
	$dataUser = getUser($idUser);
	if (count($dataUser) == 0) return false;
 
	$payload = $dataUser["pseudo"] . date("H:i:s");
	$hash = md5($payload); 
	$SQL = "UPDATE users SET hash='$hash' WHERE id='$idUser'"; 
	SQLUpdate($SQL); 
	return $hash; 
}

function mkUser($pseudo, $pass){
	$SQL = "INSERT INTO users(pseudo,pass) VALUES('$pseudo', '$pass')";
	$idUser = SQLInsert($SQL);
	mkHash($idUser); 
	return $idUser; 
}


function rmUser($idUser) {
	$SQL = "DELETE FROM users WHERE id='$idUser'";
	return SQLDelete($SQL);
}

function chgPassword($idUser,$pass) {
	$SQL = "UPDATE users SET pass='$pass' WHERE id='$idUser'";
	return SQLUpdate($SQL);
}

// ESPACES ///////////////////////////////////////////////////

function getEspaces($idUser=false){
	$SQL = "SELECT id,nom FROM espace WHERE utilisateur_id='$idUser'"; 
	return parcoursRs(SQLSelect($SQL));
}

function getEspace($idEspace, $idUser=false){
	$SQL = "SELECT id,nom FROM espace WHERE utilisateur_id='$idUser' AND id='$idEspace'"; 
	return parcoursRs(SQLSelect($SQL));
}

function editEspace($idEspace, $idUser=false, $nom){
	$SQL = "UPDATE espace SET nom = '$nom' WHERE utilisateur_id='$idUser' AND id='$idEspace'";
	return SQLUpdate($SQL);
}

function createEspace($idUser=false, $nom){
	$SQL = "INSERT INTO espace(nom,utilisateur_id) VALUES('$nom', '$idUser')";
	return SQLInsert($SQL);
}

function deleteEspace($idEspace, $idUser=false){
	$SQL = "DELETE FROM espace WHERE id='$idEspace' AND utilisateur_id='$idUser'";
	return SQLDelete($SQL);
}

function getListsUser($idUser){
	$SQL = "SELECT id,label FROM lists WHERE idUser='$idUser'"; 
	return parcoursRs(SQLSelect($SQL));
}

/////////////////////////////////////////////////////////////////////////:

// INDICATEURS ///////////////////////////////////////////

function getIndicateurs($idEspace=false){
	$SQL = "SELECT * FROM indicateur AS i
	INNER JOIN indicateur_has_type AS iht ON i.id = iht.indicateur_id
	WHERE i.espace_id='$idEspace'"; 

	return parcoursRs(SQLSelect($SQL));
}

function getIndicateur($idIndicateur, $idEspace=false){
	$SQL = $SQL = "SELECT * FROM indicateur AS i
	INNER JOIN indicateur_has_type AS iht ON i.id = iht.indicateur_id
	WHERE i.espace_id='$idEspace'
	AND i.id='$idIndicateur'"; 

	return parcoursRs(SQLSelect($SQL));
}

function createIndicateur($idEspace, $nom, $type){
	$SQL = "INSERT INTO indicateur(nom,espace_id) VALUES('$nom', '$idEspace')";
	$newIndicateurId = SQLInsert($SQL);
	
	$SQL2 = "INSERT INTO indicateur_has_type(indicateur_id,type_id) VALUES('$newIndicateurId','$type')";
	return SQLInsert($SQL2);
}

function editIndicateur($idIndicateur, $idEspace, $nom, $type){
	$SQL = "UPDATE indicateur SET nom = '$nom' WHERE espace_id='$idEspace' AND id='$idIndicateur'";
	SQLUpdate($SQL);
	
	$SQL = "UPDATE indicateur_has_type SET type_id = '$type' WHERE indicateur_id='$idIndicateur'";
	return SQLUpdate($SQL);
}

function deleteIndicateur($idIndicateur){
	$SQL = "DELETE FROM indicateur WHERE id='$idIndicateur'";
	return SQLDelete($SQL);
}

function fillIndicateur($idIndicateur, $valeur, $type, $date){
	// On selectionne d'abords tout les types existants
	$types = getTypes();

	$SQL = null;

	// todo: à améliorer
	switch ($type) {
			case 1:
				$SQL = "UPDATE indicateur_has_type SET valeur_int = '$valeur' AND 'date' = '$date' WHERE indicateur_id='$idIndicateur' AND type_id='$type'";
				break;

			case 2:
				$SQL = "UPDATE indicateur_has_type SET valeur_text = '$valeur' AND 'date' = '$date' WHERE indicateur_id='$idIndicateur' AND type_id='$type'";
				break;

			case 3:
				$SQL = "UPDATE indicateur_has_type SET valeur_date = '$valeur' AND 'date' = '$date' WHERE indicateur_id='$idIndicateur' AND type_id='$type'";
				break;

			case 4:
				$SQL = "UPDATE indicateur_has_type SET valeur_bool = '$valeur' AND 'date' = '$date' WHERE indicateur_id='$idIndicateur' AND type_id='$type'";
				break;

			case 5:
				$SQL = "UPDATE indicateur_has_type SET valeur_time = '$valeur' AND 'date' = '$date' WHERE indicateur_id='$idIndicateur' AND type_id='$type'";
				break;
			
			default:
				
				break;
		}

	return SQLUpdate($SQL);
	
}

//////////////////////////////////////////////////////////


?>
