<?php
include_once("libs/maLibUtils.php");
include_once("libs/modele.php");

$data = array("version"=>1);
// Routes : /api/...

$method = $_SERVER["REQUEST_METHOD"];
debug("method",$method);

$data["success"] = false;
$data["status"] = 400; 

// https://developers.google.com/tasks/v1/reference/

// Verif autorisation : il faut un hash
// Il peut être dans le header, ou dans la chaîne de requête

$connected = false; 

if (!($hash = valider("hash"))) 
	$hash = valider("HTTP_HASH","SERVER"); 
if($hash) {
	// Il y a un hash, il doit être correct...
	if ($connectedId = hash2id($hash)) $connected = true; 
	else {
		// non connecté - peut-être est-ce POST vers /autenticate...
		$method = "error";
		$data["status"] = 403;
	}
}



if (valider("request")) {
	$requestParts = explode('/',$_REQUEST["request"]);

	debug("rewrite-request" ,$_REQUEST["request"] ); 
	debug("#parts", count($requestParts) ); 

	$entite1 = false;
	$idEntite1 = false;
	$entite2 = false; 
	$idEntite2 = false; 

	if (count($requestParts) >0) {
		$entite1 = $requestParts[0];
		debug("entite1",$entite1); 
	} 

	if (count($requestParts) >1) {	
		if (is_id($requestParts[1])) {
			$idEntite1 = intval($requestParts[1]);
			debug("idEntite1",$idEntite1); 
		} else {
			// erreur !
			$method = "error";
			$data["status"] = 400; 
		}
	}

	if (count($requestParts) >2) {
		$entite2 = $requestParts[2];
		debug("entite2",$entite2); 
	}

	if (count($requestParts) >3) {
		if (is_id($requestParts[3])) {
			$idEntite2 = intval($requestParts[3]);
			debug("idEntite2",$idEntite2); 
		} else {
			// erreur !
			$method = "error";
			$data["status"] = 400;
		}

	}  

// TODO: en cas d'erreur : changer $method pour préparer un case 'erreur'

	$action = $method; 
	if ($entite1) $action .= "_$entite1";
	if ($entite2) $action .= "_$entite2";
 
	debug("action", $action);
	//die("$action");

	if ($action == "POST_authenticate") {
		
		if ($user = valider("user"))
		if ($password = valider("password")) {
			if ($hash = validerUser($user, $password)) {
				$data["hash"] = $hash;
				$data["success"] = true;
				$data["status"] = 200;
			} else {
				// connexion échouée
				$data["status"] = 401;
			}
		}
	}
	elseif ($connected)
	{
		// On connaît $connectedId
		switch ($action) {

			case 'GET_users' :			
				if ($idEntite1) {
					// GET /api/users/<id>
					$data["user"] = getUser($idEntite1);
					$data["success"] = true;
					$data["status"] = 200; 
				} 
				else {
					// GET /api/users

					$data["users"] = getUsers();
					$data["success"] = true;
					$data["status"] = 200;
				}
			break; 

			case 'GET_users_lists' : 
				if ($idEntite1)
				if ($idEntite2) {
					// GET /api/users/<id>/lists/<id>
					$data["list"] = getList($idEntite2, $idEntite1);
					$data["success"] = true;
					$data["status"] = 200;
				} else {
					// GET /api/users/<id>/lists
					$data["espaces"] = getListsUser($idEntite1);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;

			case 'GET_espaces' : 

				// GET /espaces
				// TODO : vérifier user ?
				$data["list"] = getEspaces($connectedId);
				$data["success"] = true;
				$data["status"] = 200;
			break;

			case 'GET_espace' : 
				if ($idEntite1) {

					$data["espace"] = getEspace($idEntite1, $connectedId);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;

			case 'POST_edit_espace' : 
				
				if ($idEspace = valider("id"))
				if ($nom = valider("nom")) {
					$data["espace"] = editEspace($idEspace, $connectedId, $nom);
					$data["success"] = true;
					$data["status"] = 200;
				}
				
			break;

			case 'POST_create_espace' : 

				if ($nom = valider("nom")) {
					$data["espace"] = createEspace($connectedId, $nom);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;

			case 'DELETE_espace' : 
				if ($idEntite1) {
					// GET /api/lists/<id>/items/<id>
					$data["espace"] = deleteEspace($idEntite1, $connectedId);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;

			case 'GET_indicateurs' : 
				if($idEntite1){
					
					$data["indicateurs"] = getIndicateurs($idEntite1);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;

			case 'GET_espace_indicateur' : 
			
				// 1:espace 2:indicateur
				if($idEntite1)
				if($idEntite2){
					
					$data["indicateur"] = getIndicateur($idEntite2, $idEntite1);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;

			case 'POST_create_indicateur' : 

				if($type = valider("type"))
				{
					if($idEspace = valider("espace")){
						if ($nom = valider("nom")) {
						$data["indicateur"] = createIndicateur($idEspace, $nom, $type);
						$data["success"] = true;
						$data["status"] = 200;
						}
					}
				}
			break;

			case 'POST_edit_indicateur' : 
				if($idEspace = valider("espace")){
					if($type = valider("type")){
						if ($idIndicateur = valider("id"))
						{
							if ($nom = valider("nom")) {
								$data["indicateur"] = editIndicateur($idIndicateur, $idEspace, $nom, $type);
								$data["success"] = true;
								$data["status"] = 200;
							}
						}
					}
				}
				
			break;

			case 'DELETE_indicateur' : 
			
				if ($idEntite1) {
					
					$data["indicateur"] = deleteIndicateur($idEntite1);
					$data["success"] = true;
					$data["status"] = 200;
				}
			break;


			case 'POST_fill_indicateur' : 

				if($date = valider("date")){
					if($type = valider("type"))
					{
						if($idIndicateur = valider("indicateur")){
							if ($valeur = valider("valeur")) {
							$data["indicateur"] = fillIndicateur($idIndicateur, $valeur, $type, $date);
							$data["success"] = true;
							$data["status"] = 200;
							}
						}
					}
				}
			break;

			case 'POST_users' : 
				// POST /api/users?pseudo=&pass=...
				if ($pseudo = valider("pseudo"))
				if ($pass = valider("pass")) {
					$id = mkUser($pseudo, $pass); 
					$data["user"] = getUser($id);
					$data["success"] = true;
					$data["status"] = 201;
				}
			break; 

			case 'POST_users_lists' :
				// POST /api/users/<id>/lists?label=...
				if ($idEntite1)
				if ($label = valider("label")) {
					$id = mkList($idEntite1, $label); 
					$data["list"] = getList($id);
					$data["success"] = true;
					$data["status"] = 201;
				}
			break; 

			case 'POST_lists_items' :
				// POST /api/lists/<id>/items?label=...
				if ($idEntite1)
				if ($label = valider("label")) {
					$url = valider("url"); 
					$id = mkItem($idEntite1, $label,$url);					
					$data["item"] = getItem($id,$idEntite1);
					$data["success"] = true; 
					$data["status"] = 201;
				}
			break; 

			case 'POST_lists' :
				// POST /api/lists?label=...
				if ($label = valider("label")) {
					$id = mkList($connectedId, $label); 
					$data["list"] = getList($id);
					$data["success"] = true; 
					$data["status"] = 201;
				}
			break;

			case 'PUT_authenticate' : 
				// régénère un hash ? 
				$data["hash"] = mkHash($connectedId); 
				$data["success"] = true; 
				$data["status"] = 200;
			break; 

			case 'PUT_users' :
				// PUT  /api/users/<id>?pass=...
				if ($idEntite1)
				if ($pass = valider("pass")) {
					if (chgPassword($idEntite1,$pass)) {
						$data["user"] = getUser($idEntite1);
						$data["success"] = true; 
						$data["status"] = 200;
					} else {
						// erreur 
					}
				}
			break; 

			case 'PUT_users_lists' : 
				// PUT /api/users/<id>/lists/<id>?label=...
				if ($idEntite1)
				if ($idEntite2)
				if ($label = valider("label")) {
					if (chgListLabel($idEntite2,$label,$idEntite1)) {
						$data["list"] = getList($idEntite2);
						$data["success"] = true; 
						$data["status"] = 200;
					} else {
						// erreur
					}
				}
			break; 

			case 'PUT_lists' : 
				// PUT /api/lists/<id>?label=...
				if ($idEntite1)
				if ($label = valider("label")) {
					if (chgListLabel($idEntite1,$label,$connectedId)) {
						$data["list"] = getList($idEntite1);
						$data["success"] = true; 
						$data["status"] = 200;
					} else {
						// erreur
					}
				}
			break; 

			case 'PUT_lists_items' : 
				// PUT /api/lists/<id>/items/<id>?label=...
				if ($idEntite1)
				if ($idEntite2)
				if ($label = valider("label")) {
					if (chgItemLabel($idEntite2,$label,$idEntite1)) {
						$data["item"] = getItem($idEntite2,$idEntite1);
						$data["success"] = true; 
						$data["status"] = 200;
					} else {
						// erreur
					}
				}

				// PUT /api/lists/<id>/items/<id>?url=...
				if ($idEntite1)
				if ($idEntite2)
				if ($url = valider("url")) {
					if (chgItemUrl($idEntite2,$url,$idEntite1)) {
						$data["item"] = getItem($idEntite2,$idEntite1);
						$data["success"] = true;
						$data["status"] = 200; 
					} else {
						// erreur
					}
				}

				// PUT /api/lists/<id>/items/<id>?check=0|1
				if ($idEntite1)
				if ($idEntite2){
					$check = valider("check"); 	// 0 est valide !
					if ($check !== false) {		// false, non...
						$check =intval($check); 
						if (is_check($check)) {
							if (checkItem($idEntite2,$idEntite1, $check)) {
								$data["item"] = getItem($idEntite2,$idEntite1);
								$data["success"] = true;
								$data["status"] = 200;
							} else {
								// erreur
							}
						}
					}
				}
			break;

			case 'DELETE_users' : 
				// DELETE /api/users/<id> 
				if ($idEntite1) {
					if (rmUser($idEntite1)) {
						$data["success"] = true;
						$data["status"] = 200;
					} else {
						// erreur 
					} 
				}
			break; 

			case 'DELETE_users_lists' : 
				// DELETE /api/users/<id>/lists/<id>
				if ($idEntite1)
				if ($idEntite2) {
					if (rmList($idEntite2, $idEntite1)) {				
						$data["success"] = true;
						$data["status"] = 200; 
					} else {
						// erreur 
					}
				}
			break; 

			case 'DELETE_lists' : 
				// DELETE /api/lists/<id>
				if ($idEntite1) {
					if (rmList($idEntite1, $connectedId)) {				
						$data["success"] = true;
						$data["status"] = 200; 
					} else {
						// erreur 
					}
				}
			break; 

			case 'DELETE_lists_items' : 
				// DELETE /api/lists/<id>/items/<id>
				if ($idEntite1)
				if ($idEntite2) {
					if (rmItem($idEntite2, $idEntite1)) {				
						$data["success"] = true;
						$data["status"] = 200;  
					} else {
						// erreur 
					}
				}
			break; 
		} // switch(action)
	} //connected
}

switch($data["status"]) {
	case 200: header("HTTP/1.0 200 OK");	break;
	case 201: header("HTTP/1.0 201 Created");	break; 
	case 202: header("HTTP/1.0 202 Accepted");	break;
	case 204: header("HTTP/1.0 204 No Content");	break;
	case 400: header("HTTP/1.0 400 Bad Request");	break; 
	case 401: header("HTTP/1.0 401 Unauthorized");	break; 
	case 403: header("HTTP/1.0 403 Forbidden");	break; 
	case 404: header("HTTP/1.0 404 Not Found");		break;
	default: header("HTTP/1.0 200 OK");
		
}

echo json_encode($data);

?>
